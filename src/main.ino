#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiClientSecureBearSSL.h>

#include <PN532.h>
#include <PN532_I2C.h>
#include <Wire.h>

#include "secrets.h"

#define UID_SIZE 7
#define UID_SIZE_ASCII 15

#define HAPTIC D8

PN532_I2C pn532i2c(Wire);
PN532 nfc(pn532i2c);

ESP8266WiFiMulti WiFiMulti;
uint8_t lastUid[UID_SIZE] = {0};

void haptic() {
  digitalWrite(HAPTIC, HIGH);
  delay(300);
  digitalWrite(HAPTIC, LOW);
}

void setup(void) {
  Serial.begin(115200);
  Serial.printf("Hello!\n");
  pinMode(HAPTIC, OUTPUT);
  digitalWrite(HAPTIC, LOW);

  Wire.setClockStretchLimit(2000);

  uint32_t versiondata;
  while (1) {
    nfc.begin();
    versiondata = nfc.getFirmwareVersion();
    if (versiondata) {
      break;
    } else {
      Serial.printf("Didn't find PN53x board\n\n");
      delay(500);
    }
  }

  // Got ok data, print it out!
  Serial.print("Found chip PN5");
  Serial.println((versiondata >> 24) & 0xFF, HEX);
  Serial.print("Firmware ver. ");
  Serial.print((versiondata >> 16) & 0xFF, DEC);
  Serial.print('.');
  Serial.println((versiondata >> 8) & 0xFF, DEC);

  // configure board to read RFID tags
  nfc.SAMConfig();

  WiFiMulti.addAP(ssid, password);

  if ((WiFiMulti.run() != WL_CONNECTED)) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("Waiting for an ISO14443A Card ...");
  haptic();
  delay(100);
  haptic();
}

void postUid(char * uidString) {
  haptic();
  std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);
  client->setInsecure();

  HTTPClient https;
  https.begin(*client, update_url);
  https.addHeader("Content-Type", "application/json");
  String json = String("{\"uid\":\"") + String(uidString) + String("\"}");
  int httpCode = https.POST(json);

  // httpCode will be negative on error
  if (httpCode > 0) {
    // HTTP header has been send and Server response header has been handled
    Serial.printf("[HTTPS] GET... code: %d\n", httpCode);

    // file found at server
    if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
      String payload = https.getString();
      Serial.println(payload);
      haptic();
    }
  } else {
    Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
  }

  https.end();
  Serial.println("closing connection");
}

void loop(void) {
  bool success;
  uint8_t uid[UID_SIZE] = {0};
  uint8_t uidLength;
  char uidString[UID_SIZE_ASCII] = {0};

  nfc.SAMConfig();

  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, &uid[0], &uidLength);
  Serial.printf("\n");
  if (success) {
    Serial.printf("Found a card!\n");

    Serial.printf("UID Length: %d bytes\n", uidLength);
    Serial.printf("UID Value: ");
    //int id = 0; //create a variable to build the user id into
    for (uint8_t i = 0; i < uidLength; i++) {
      Serial.printf("%02x", uid[i]);
      sprintf(uidString + (2 * i), "%02x", uid[i]);
    }
    Serial.printf("\n");

    if (memcmp(uid, lastUid, uidLength) != 0) {
      memcpy(lastUid, uid, uidLength);
      postUid(uidString);
    }

    delay(1000);
  } else {
    // PN532 probably timed out waiting for a card
    Serial.printf("\nTimed out waiting for a card\n");
    delay(200);
  }
}
