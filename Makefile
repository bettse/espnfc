# Uncomment lines below if you have problems with $PATH
SHELL := /usr/local/bin/zsh
#PATH := /usr/local/bin:$(PATH)

DEVICE=/dev/cu.usbserial-0001


all:
	platformio -c vim run

format:
	clang-format -style=file -i src/*

upload:
	platformio -c vim run --target upload --upload-port ${DEVICE}

clean:
	platformio -c vim run --target clean

program:
	platformio -c vim run --target program

uploadfs:
	platformio -c vim run --target uploadfs

update:
	platformio -c vim update

monitor:
	platformio device monitor -b 115200 -p ${DEVICE}
